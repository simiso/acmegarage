package za.co.absa.acmegarage;

import org.junit.Test;
import za.co.absa.acmegarage.domain.Item;
import za.co.absa.acmegarage.domain.OrderItem;
import za.co.absa.acmegarage.domain.TaxClass;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class OrderItemTest {

    @Test
    public void shouldCalculateVat() throws Exception {
        BigDecimal itemPrice = new BigDecimal("100.00");
        Item item = new Item("Item", TaxClass.VALUE_ADDED_TAX);
        OrderItem orderItem = new OrderItem(1,item,itemPrice);
        BigDecimal expectedVAT = new BigDecimal("14.00");
        BigDecimal calculatedVat = orderItem.calculateTax();
        assertEquals(expectedVAT, calculatedVat);
    }

    @Test
    public void shouldCalculateImportTax() throws Exception {
        BigDecimal itemPrice = new BigDecimal("100.00");
        Item item = new Item("Item",TaxClass.IMPORT_TAX);
        OrderItem orderItem = new OrderItem(2,item,itemPrice);
        BigDecimal expectedImportTax = new BigDecimal("20.00");
        BigDecimal calculatedImportTax = orderItem.calculateTax();
        assertEquals(expectedImportTax, calculatedImportTax);
    }
    @Test
    public void shouldCalculateNoTax() throws Exception {
        BigDecimal itemPrice = new BigDecimal("100.00");
        Item item = new Item("Item",TaxClass.NO_TAX);
        OrderItem orderItem = new OrderItem(1,item,itemPrice);
        BigDecimal expectedNoTax = new BigDecimal("00.00");
        BigDecimal calculatedNoTax = orderItem.calculateTax();
        assertEquals(expectedNoTax, calculatedNoTax);
    }

    @Test
    public void shouldCalculateTaxPriceVatInclusive() throws Exception {
        BigDecimal itemPrice = new BigDecimal("100.00");
        Item item = new Item("Item",TaxClass.VALUE_ADDED_TAX);
        OrderItem orderItem = new OrderItem(2,item,itemPrice);
        BigDecimal expectedTaxPrice = new BigDecimal("228.00");
        BigDecimal calculatedTaxPrice = orderItem.calculateTaxPrice();
        assertEquals(expectedTaxPrice, calculatedTaxPrice);
    }
    @Test
    public void shouldCalculateTaxPriceImportInclusive() throws Exception {
        BigDecimal itemPrice = new BigDecimal("100.00");
        Item item = new Item("Import Item",TaxClass.IMPORT_TAX);
        OrderItem orderItem = new OrderItem(1,item,itemPrice);
        BigDecimal expectedTaxPrice = new BigDecimal("110.00");
        BigDecimal calculatedTaxPrice = orderItem.calculateTaxPrice();
        assertEquals(expectedTaxPrice, calculatedTaxPrice);
    }
    @Test
    public void shouldCalculateTaxPriceNoTax() throws Exception {
        BigDecimal itemPrice = new BigDecimal("100.00");
        Item item = new Item("Item",TaxClass.NO_TAX);
        OrderItem orderItem = new OrderItem(1,item,itemPrice);
        BigDecimal expectedTaxPrice = new BigDecimal("100.00");
        BigDecimal calculatedTaxPrice = orderItem.calculateTaxPrice();
        assertEquals(expectedTaxPrice, calculatedTaxPrice);
    }
}