package za.co.absa.acmegarage;

import za.co.absa.acmegarage.domain.Item;
import za.co.absa.acmegarage.domain.TaxClass;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ItemTest {

    @org.junit.Test
    public void canCreateItem() throws Exception {
        BigDecimal itemPrice = new BigDecimal("100.00");
        Item item = new Item("Item", TaxClass.VALUE_ADDED_TAX);
        assertTrue(item.getItemName().equals("Item"));
    }

}