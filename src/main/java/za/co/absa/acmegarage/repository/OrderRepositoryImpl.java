package za.co.absa.acmegarage.repository;

import za.co.absa.acmegarage.database.FakeDatabase;
import za.co.absa.acmegarage.domain.Order;

import java.util.Collection;


public class OrderRepositoryImpl implements OrderRepository {
    @Override
    public void save(Order order) {
        FakeDatabase.save(order);
    }

    @Override
    public Order find(String orderName) {
        return FakeDatabase.find(orderName);
    }

    @Override
    public Collection<Order> findAll() {
        return FakeDatabase.findAll();
    }
}
