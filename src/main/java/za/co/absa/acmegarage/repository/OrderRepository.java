package za.co.absa.acmegarage.repository;

import za.co.absa.acmegarage.domain.Order;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public interface OrderRepository {
    public static List<String> NO_TAX_ITEMS = Arrays.asList("Bread", "Eggs");

    void save(Order order);

    Order find(String orderName);

    Collection<Order> findAll();
}
