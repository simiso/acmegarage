package za.co.absa.acmegarage.domain;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Order {

    private String orderName;
    private Set<OrderItem> orderItems;

    public Order(String orderName) {
        this.orderName = orderName;
        this.orderItems = new HashSet<>();
    }

    public BigDecimal calculateTaxAmount() {
        return orderItems.stream().map(OrderItem::calculateTax).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal calculateTotalPrice() {
        return orderItems.stream().map(OrderItem::calculateTaxPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public void addOrderItem(OrderItem orderItem) {
        this.orderItems.add(orderItem);
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<Item> orderItems) {
        this.orderItems = this.orderItems;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        return orderName.equals(order.orderName);
    }

    @Override
    public int hashCode() {
        return orderName.hashCode();
    }

    public String receiptString() {
        StringBuilder format = new StringBuilder(orderName + "\n");
        for (OrderItem orderItem : orderItems) {
            format.append(orderItem.getQuantity()).append(" ").append(orderItem.getItem().getItemName()).append(" ").append(orderItem.getPrice()).append("\n");
        }
        format.append("Tax amount: ").append(calculateTaxAmount()).append("\n");
        format.append("Total: ").append(calculateTotalPrice()).append("\n");
        return format.toString();
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderName='" + orderName + '\'' +
                ", orderItems=" + orderItems +
                '}';
    }
}
