package za.co.absa.acmegarage.domain;

import java.math.BigDecimal;

public enum TaxClass {
    VALUE_ADDED_TAX(new BigDecimal("0.14")),
    IMPORT_TAX(new BigDecimal("0.10")),
    NO_TAX(BigDecimal.ZERO);

    private BigDecimal value;

    private TaxClass(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }
}
