package za.co.absa.acmegarage.domain;

import java.math.BigDecimal;

public class OrderItem {
    private Integer quantity;
    private Item item;
    private BigDecimal price;

    public OrderItem(Integer quantity, Item item, BigDecimal price) {
        this.quantity = quantity;
        this.item = item;
        this.price = price;
    }

    public BigDecimal calculateTax() {
        return this.price.multiply(BigDecimal.valueOf(quantity)).multiply(item.getTaxClass().getValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    public BigDecimal calculateTaxPrice() {
        return this.price.multiply(BigDecimal.valueOf(quantity)).add(calculateTax()).setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderItem)) return false;

        OrderItem orderItem = (OrderItem) o;

        return item.equals(orderItem.item);
    }

    @Override
    public int hashCode() {
        return item.hashCode();

    }
}
