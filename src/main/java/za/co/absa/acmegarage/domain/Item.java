package za.co.absa.acmegarage.domain;

public class Item {

    private String itemName;
    private TaxClass taxClass;

    public Item(String itemName, TaxClass taxClass) {
        this.itemName = itemName;
        this.taxClass = taxClass;
    }

    public TaxClass getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(TaxClass taxClass) {
        this.taxClass = taxClass;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;

        Item item = (Item) o;

        return itemName.equals(item.itemName);
    }

    @Override
    public int hashCode() {
        return itemName.hashCode();
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemName='" + itemName + '\'' +
                ", taxClass=" + taxClass +
                '}';
    }
}
