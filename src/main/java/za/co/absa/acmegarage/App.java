package za.co.absa.acmegarage;

import za.co.absa.acmegarage.domain.Item;
import za.co.absa.acmegarage.domain.Order;
import za.co.absa.acmegarage.domain.OrderItem;
import za.co.absa.acmegarage.domain.TaxClass;
import za.co.absa.acmegarage.repository.OrderRepository;
import za.co.absa.acmegarage.repository.OrderRepositoryImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static za.co.absa.acmegarage.repository.OrderRepository.NO_TAX_ITEMS;

public class App {
    private final OrderRepository orderRepository;
    private String currentOrder = null;
    public App(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public static void main(String[] args) throws IOException {
        Stream<String> lines = null;
        if (args.length == 1) {
            lines = Files.lines(Paths.get(args[0]));
        } else {
            String orders = "Order 3\n" +
                    "Quantity    Item                   Price\n" +
                    "1           Eggs                   20.00\n" +
                    "1           Imported Chocolates    42.99\n" +
                    "1           Matches                1.20";
            orders = orders + "\nOrder 2\n" +
                    "Quantity    Item                Price\n" +
                    "1           Newspaper           15.50\n" +
                    "1           Imported Magazine   65.99";
            orders = orders + "\nOrder 1\n" +
                    "Quantity    Item             Price\n" +
                    "1           Bread            12.99\n" +
                    "1           Magazine         34.49";

            List<String> rows = Arrays.asList(orders.split("\n"));
            lines = rows.stream();
        }
        OrderRepository orderRepository = new OrderRepositoryImpl();
        App app = new App(orderRepository);
        app.parse(lines);
        app.printOrders();
    }

    private void parse(Stream<String> lines) throws IOException {
        lines.filter(line ->
                line.matches("^(\\d+)?(\\s+[Imported]*\\s*\\w+\\s+)?(\\d+\\.{0,1}\\d+$)") || line.matches("^(Order\\s+\\d$)")
        ).forEach(this::readLines);
        lines.close();
    }

    private void readLines(String line) {
        String[] lineTokes = line.trim().split("\\s+");
        if (lineTokes.length == 2) {
            createOrder(line);
        } else if (lineTokes.length == 4) {
            addItem(lineTokes[0], lineTokes[1] + " " + lineTokes[2], lineTokes[3]);
        } else if (lineTokes.length == 3) {
            addItem(lineTokes[0], lineTokes[1], lineTokes[2]);
        }
    }

    private void addItem(String quantity, String itemName, String price) {
        Integer quantityInt = Integer.parseInt(quantity);
        Item item = new Item(itemName, getItemType(itemName));
        BigDecimal priceValue = new BigDecimal(price);
        Order order = orderRepository.find(currentOrder);
        order.addOrderItem(new OrderItem(quantityInt, item, priceValue));
        orderRepository.save(order);
    }

    public void createOrder(String currentOrder) {
        this.currentOrder=currentOrder;
        orderRepository.save(new Order(currentOrder));
    }

    private void printOrders() throws IOException {
        orderRepository.findAll().forEach(order -> System.out.println(order.receiptString()));
    }

    private TaxClass getItemType(String itemName) {
        if (NO_TAX_ITEMS.contains(itemName))
            return TaxClass.NO_TAX;
        else if (itemName.contains("Imported ")) {
            return TaxClass.IMPORT_TAX;
        } else {
            return TaxClass.VALUE_ADDED_TAX;
        }
    }

}