package za.co.absa.acmegarage.database;

import za.co.absa.acmegarage.domain.Order;

import java.util.*;

public class FakeDatabase {

   private static Map<String, Order> orderTable;
    static {
        orderTable = new HashMap<>();
    }

    public static void save(Order order){
        if (order==null||order.getOrderName().isEmpty()){
            throw new RuntimeException("Order cannot be saved: "+order);
        } else {
            orderTable.put(order.getOrderName(), order);
        }
    }

    public static Order find(String orderName){
        if(orderTable.containsKey(orderName)) {
            return orderTable.get(orderName);
        } else {
            throw new RuntimeException("Cant find order: "+orderName);
        }
    }

    public static Collection<Order> findAll(){
        return orderTable.values();
    }


}
