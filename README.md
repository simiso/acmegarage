## ACME garage

This project is a java maven project.

## Building
Requires: 
    Java 1.8
    mvn
Run mvn clean install.

Tests will run and produce a jar file in target folder then run java -jar acmegarage-1.0-SNAPSHOT.jar classes/Orders.txt.

App is the main class read and parse file.

Item -  items that can be sold.

OrderItem - OrderItem entity in the context of ordering.

Order - aggregate root for all the aggregates members in the context fo ordering {OrderItem, Item, TaxClass }.

TaxClass - value object for VAT and Import Tax.


Made dummy repository and fake database to save orders.

